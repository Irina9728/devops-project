import logo from './logo.svg';
import './App.css';
import * as Sentry from "@sentry/react";
import {Integrations} from "@sentry/tracing";

Sentry.init({
    dsn: "https://5b41f899711f43f9965469718cf61de6@o850568.ingest.sentry.io/5817563",
    integrations: [new Integrations.BrowserTracing()],

    tracesSampleRate: 1.0,
});

function App() {
    return (
        <div className="App">
            <header className="App-header">
                <img src={logo} className="App-logo" alt="logo"/>
                <a
                    className="App-link react"
                    href="https://reactjs.org"
                    target="_blank"
                    rel="noopener noreferrer"
                >
                    Learn React
                </a>
                <p> Before learning react you should also know</p>
                <ol>
                    <li>
                        Javascript
                        <div>
                            <a className="App-link js"
                               href="https://learnjavascript.online"
                               target="_blank"
                               rel="noopener noreferrer"
                            >
                                Learn Javascript
                            </a>
                        </div>
                    </li>
                    <li>
                        HTML
                        <div>
                            <a className="App-link html"
                               href="https://www.w3schools.com/html/"
                               target="_blank"
                               rel="noopener noreferrer"
                            >
                                Learn HTML
                            </a>
                        </div>
                    </li>
                    <li>
                        CSS
                        <div>
                            <a className="App-link css"
                               href="https://web.dev/learn/css/"
                               target="_blank"
                               rel="noopener noreferrer"
                            >
                                Learn CSS
                            </a>
                        </div>
                    </li>
                </ol>
                <p> After learning basics React you may be interested in more advanced topics </p>
                <ul>
                    <li>
                        <a className="App-link js"
                           href="https://reactjs.org/docs/accessibility.html"
                           target="_blank"
                           rel="noopener noreferrer"
                        >
                            Learn Advanced React
                        </a>
                    </li>
                </ul>
            </header>
        </div>
    );
}

export default App;
