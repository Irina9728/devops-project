import { render, screen } from '@testing-library/react';
import App from './App';

test('renders learn react link', () => {
  render(<App />);
  const linkElement = screen.getByText(/learn react/i);
  expect(linkElement).toBeInTheDocument();
});

test('renders learn advanced react link', () => {
  render(<App />);
  const linkElement = screen.getByText(/learn advanced react/i);
  expect(linkElement).toBeInTheDocument();
});

test('renders learn js link', () => {
  render(<App />);
  const linkElement = screen.getByText(/learn javascript/i);
  expect(linkElement).toBeInTheDocument();
});

test('renders learn css link', () => {
  render(<App />);
  const linkElement = screen.getByText(/learn css/i);
  expect(linkElement).toBeInTheDocument();
});
test('renders learn html link', () => {
  render(<App />);
  const linkElement = screen.getByText(/learn html/i);
  expect(linkElement).toBeInTheDocument();
});
