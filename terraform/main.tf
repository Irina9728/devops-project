terraform {
  backend "http" {}
  required_providers {
    gitlab = {
      source= "gitlabhq/gitlab"
      version = "~> 3.1"
    }
    heroku = {
      source  = "heroku/heroku"
      version = "~> 4.0"
    }
  }
}

variable "gitlab_access_token" {
  type = string
}

variable "HEROKU_API_KEY" {
  type = string
}

variable "HEROKU_USER" {
  type = string
}
provider "gitlab" {
  token = var.gitlab_access_token
}

provider "heroku" {
  email   = var.HEROKU_USER
  api_key = var.HEROKU_API_KEY
}

data "gitlab_project" "devops-project" {
  id = 27359744
}

//the following two are environment variables that allow the CI scripts to push to Heroku

resource "gitlab_project_variable" "HEROKU_API_KEY" {
  key = "HEROKU_API_KEY"
  project = data.gitlab_project.devops-project.id
  value = "51aaf7c7-73de-41fd-bf1c-d7c5db077866"
}

resource "gitlab_project_variable" "HEROKU_USER" {
  key = "HEROKU_USER"
  project = data.gitlab_project.devops-project.id
  value = "heroku"
}

resource "heroku_app" "dev-app" {
  name   = "devops-project-dev"
  region = "us"
}

resource "heroku_app" "prod-app" {
  name   = "devops-project-prod"
  region = "us"
}

resource "heroku_app" "qa-app" {
  name   = "devops-project-qa"
  region = "us"
}
